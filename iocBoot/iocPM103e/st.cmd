#!../../bin/linux-x86_64/PM103e

#- You may have to change PM103e to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/PM103e.dbd"
PM103e_registerRecordDeviceDriver pdbbase

epicsEnvSet ("STREAM_PROTOCOL_PATH", "${TOP}/db")

drvAsynIPPortConfigure ("PM103E", "192.168.178.35:2000")

## Load record instances
dbLoadRecords("db/PM103e.db","PORT=PM103E, P=THOR")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=laurenz"
